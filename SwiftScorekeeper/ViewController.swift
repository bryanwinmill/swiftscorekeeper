//
//  ViewController.swift
//  SwiftScorekeeper
//
//  Created by Bryan Winmill on 9/12/17.
//  Copyright © 2017 Bryan Winmill. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var teamOneName: UITextField!
    
    @IBOutlet var teamTwoName: UITextField!

    
    @IBAction func firstStepper(_ sender: UIStepper) {
        
        teamOneScore.text = String(format: "%.0f", sender.value)
    }
    
    @IBOutlet var teamOneScore: UILabel!
    
    
    @IBAction func secondStepper(_ sender: UIStepper) {
        
        teamTwoScore.text = String(format: "%.0f", sender.value)
    }
    
    @IBOutlet var teamTwoScore: UILabel!
    
    
    @IBAction func resetScore(_ sender: UIButton) {
        
        teamOneScore.text = "0"
        teamTwoScore.text = "0"
        resetStepperOne.value = 0;
        resetStepperTwo.value = 0;
    
        
    }

    @IBOutlet var resetStepperOne: UIStepper!
    
    @IBOutlet var resetStepperTwo: UIStepper!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.teamOneName.delegate = self
        self.teamTwoName.delegate = self
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        teamOneName.resignFirstResponder()
        teamTwoName.resignFirstResponder()
        
        return true
    }


}

